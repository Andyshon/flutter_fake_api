import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterfakeapi/Constants.dart';

import 'models.dart';

class PostDetail extends StatefulWidget {

  final Post post;

  PostDetail(this.post);

  @override
  _DetailsState createState() => _DetailsState();

}

class _DetailsState extends State<PostDetail> {

  Post post;

  @override
  void initState() {
    post = widget.post;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => Navigator.pop(context)
          ),
          title: Text(post_detail),
        ),
        body: _buildInfo(context)
    );
  }

  Widget _buildInfo(BuildContext context) {

      return Container(
            padding: EdgeInsets.only(left: 15, right: 20, top: 15, bottom: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                  Flexible(
                    child: Text(
                      post.title.replaceRange(0, 1, post.title[0].toUpperCase()),
                      style: TextStyle(fontSize: 16,
                        shadows: [
                          Shadow(
                            blurRadius: 10.0,
                            color: Colors.blue,
                            offset: Offset(2.0, 2.0),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Text('id:'+post.id.toString(), style: TextStyle(fontSize: 12,color: Colors.grey),)
                ],),
                Padding(padding: const EdgeInsets.all(5.0)),
                Text('Description: ' + post.body, style: TextStyle(fontSize: 16),),
                Padding(padding: const EdgeInsets.all(5.0)),
              ],
            ),
          );
  }

}