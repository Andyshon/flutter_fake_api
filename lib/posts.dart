import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutterfakeapi/Constants.dart';
import 'package:flutterfakeapi/common.dart';
import 'package:flutterfakeapi/models.dart';
import 'package:flutterfakeapi/postDetail.dart';
import 'package:http/http.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();

  _MyHomePageState() {
    load(api_get_posts);
  }

  List<Post> posts = List();

  void load(String url) async {
    Response response;
    try {
      response = await get(url);
    }
    catch (ex) {
      showSnack(key, msg_error_load_check_url);
    }
    if (response.statusCode == 200) {
      Post post;
      List<dynamic> list;
      posts.clear();
      list = json.decode(response.body);
      list.forEach((element) {
        if (posts.length >= 2) return;
        post = Post.fromJson(element);
        setState(() {
          posts.add(post);
        });
      });
    }
    else {
      showSnack(key, msg_error_load);
    }
  }

  void remove(Post post) {
    setState(() {
      posts.remove(post);
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 5, right: 20.0),
            child: Wrap(
              children: <Widget>[
                IconButton(icon: Icon(Icons.refresh), onPressed: () {
                  load(api_get_posts);
                }),
                IconButton(icon: Icon(Icons.delete_forever), onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      Widget cancelButton = FlatButton(
                        child: Text(msg_delete),
                        onPressed: () {
                          Navigator.pop(context);
                          setState(() {
                            posts.clear();
                          });
                        },
                      );
                      Widget continueButton = FlatButton(
                        child: Text(msg_cancel),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      );
                      return AlertDialog(
                        title: Text(msg_delete),
                        content: Text(delete_posts_confirm),
                        actions: [
                          cancelButton,
                          continueButton,
                        ],
                      );
                    },
                  );
                }),
              ],
            ),
          )
        ],
        title: Text(posts_title),
      ),
      body: _myListView(context),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
//          Navigator.of(context).push(
//              MaterialPageRoute<void>(
//                builder: (BuildContext context) {
//                  return AddNewPost();
//                },
//              )
//          );
        showToast(coming_soon);
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.blue,
      ),
    );
  }

  Widget _myListView(BuildContext context) {

    final Iterable<Widget> rows = posts.map((Post post) {
      return GestureDetector(
        onTap: ()=> Navigator.of(context).push(
            MaterialPageRoute<void>(
              builder: (BuildContext context) {
                return PostDetail(post);
              },
            )
        ),
        child: Card(
          margin: EdgeInsets.all(10.0),
          child: Container(
            padding: EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                  Flexible(
                    child: Text(
                      post.title.replaceRange(0, 1, post.title[0].toUpperCase()),
                      style: TextStyle(fontSize: 16,
                        shadows: [
                          Shadow(
                            blurRadius: 10.0,
                            color: Colors.blue,
                            offset: Offset(2.0, 2.0),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Text('id:'+post.id.toString(), style: TextStyle(fontSize: 12,color: Colors.grey),)
                ],),
                Padding(padding: const EdgeInsets.all(5.0)),
                Text('Description: ' + post.body, style: TextStyle(fontSize: 16),),
                Padding(padding: const EdgeInsets.all(5.0)),
              ],
            ),
          ),
        ),
      );
    },
    );

    return ListView.builder(
      itemCount: rows.length,
      itemBuilder: (context, index) {
        return rows.toList()[index];
      },
    );
  }
}
