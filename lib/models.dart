import 'package:json_annotation/json_annotation.dart';

part 'models.g.dart';

@JsonSerializable()
class Post extends Object with _$PostSerializerMixin {
  Post(this.id, this.userId, this.title, this.body);

  int id;
  int userId;
  String title;
  String body;

  factory Post.fromJson(Map<String, dynamic> json) => _$PostFromJson(json);

  Post.fromMap(Map<String, dynamic> json) {
    id = json['id'] as int;
    userId = json['userId'] as int;
    title = json['title'] as String;
    body = json['body'] as String;
  }
}