part of 'models.dart';

abstract class _$PostSerializerMixin {
  int get id;
  int get userId;
  String get title;
  String get body;
  Map<String, dynamic> toJson() => <String, dynamic>{
    'id': id,
    'userId': userId,
    'title': title,
    'body': body
  };
}

Post _$PostFromJson(Map<String, dynamic> json) {
  return new Post(
      json['id'] as int,
      json['userId'] as int,
      json['title'] as String,
      json['body'] as String
  );
}