import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void showSnack(GlobalKey<ScaffoldState> key, String msg,
    {SnackBarAction action, int secondsDuration = 7}) =>
    key.currentState.showSnackBar(SnackBar(
      duration: Duration(seconds: secondsDuration),
      content: Text(msg),
      action: action,
    ));

void showToast(String title) {
  Fluttertoast.showToast(
      msg: title,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIos: 1,
      backgroundColor: Colors.grey,
      textColor: Colors.white,
      fontSize: 14.0
  );
}