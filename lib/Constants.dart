const String posts_title = "Posts";
const String post_detail = "Post detail";
const String msg_delete = "Delete";
const String msg_cancel = "Cancel";
const String msg_error_load = "Error loading data";
const String msg_error_load_check_url = "Error loading data. Check your URL";
const String delete_posts_confirm = "Do you want to delete all posts?";

const String api_get_posts = "https://jsonplaceholder.typicode.com/posts";

const String coming_soon = "Coming soon";